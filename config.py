from db.models import *
from db.migrate import *

from playhouse.shortcuts import *

from constants import *

import requests

import sys
from datetime import date
import time
import random
import hashlib
import json
import pickle


def helper_create_token():
	md5 = hashlib.md5()
	curr_time = str(time.time())
	rand_str = "".join(random.sample(curr_time,len(curr_time)))
	md5.update(rand_str.encode())
	return md5.hexdigest().upper()



def command_list():
	print("command list")
	commands = "createbasicdetails\nmodifybasicdetails\ncreatepumps\nsyncbasicdata\nsyncpumpdata"
	print(commands)


def run_command_from_arg(arg):
	if arg == "createbasicdetails" and server_authenticate() is True:
		create_basic_details()
	elif arg == "modifybasicdetails" and server_authenticate() is True:
		modify_basic_details()
	elif arg == "createpumps" and server_authenticate() is True:
		create_pumps()
	elif arg == "syncbasicdata" and server_authenticate() is True:
		sync_basic_data()
	elif arg == "syncpumpdata" and server_authenticate() is True:
		sync_pump_data()

	else:
		print("unknown command")
		command_list()
		sys.exit(1)
	


def server_authenticate():
	username = input("Enter your Username as a Trident Admin: ")
	password = input("Enter your password as a Trident Admin: ")
	print("Attempting to Verify Details...")
	certificate = (CERT, KEY)
	payload = {'username': username, 'password':password}
	try:
		result = requests.post(ADMIN_AUTH_SERVER+AUTH_ROUTE,json=payload, cert=certificate,verify=False)
	except Exception as e:
		print(e)
		print("Error connecting to Authentication server. Please Try again")
		sys.exit(1)

	j_result = result.json()
	if j_result['auth'] == 'false':
		print("Incorrect Username or Password")
		sys.exit(1)
	elif j_result['auth'] == 'true':
		print("Authentication was successful. Welcome back admin")
		return True


def create_basic_details():
	#first check if basic details has already been created
	print("Create basic details")

	station_name = input("Enter Name of Filling Station(e.g Total Filling Station): ")
	address = input("Enter address of Filling Station: ")
	state = input("Enter the State filling station is located in(e.g Lagos): ")
	city = input("Enter the city filling station is located in(e.g Ikeja): ")
	branch_name = input("Enter branch filling station is located in e.g(Lagos island branch): ")
	phone_number = input("Enter phone number of filling station e.g(08031234567, 01-787727): ")
	number_of_pump = int(input("Enter number of pumps/dispensers (e.g 5. Please make sure you enter an integer): "))
	contact_person = input("Enter  name of contact person: ")
	contact_person_email = input("Enter email address of contact person: ")
	com_port_pts = int(input("Enter COM port number (e.g 6 - make sure you enter a valid integer): "))
	token_permanent = helper_create_token()
	date_now = input("Enter 'y' without quotes to use current date as date the usage of this software started or hit thr return or enter key to manually enter date: ")
	if date_now == 'y' or date_now == 'Y':
		date_started = date.today()

	else:
		year = int(input("Enter the year the usage of this software started (e.g 2015 - please make sure you enter a valid integer) : "))
		month = int(input("Enter the month the usage of this software started (e.g 12, 1, please use digits to represent months): "))
		day = int(input("Enter the day the usage of this software started (e.g 1,2,30,22): "))
		try:
			date_started = date(year, month, day)
		except Exception as e:
			print(e)
			date_started = date.today()
	try:
		Basic_details.create(station_name=station_name,address=address,state=state,city=city,branch_name=branch_name,phone_number=phone_number,number_of_pump=number_of_pump,contact_person=contact_person,contact_person_email=contact_person_email,com_port_pts=com_port_pts,token_permanent=token_permanent,date_started=date_started)
	except Exception as e:
		print(e)
		print("Encountered an error creating details. Please try again")
		sys.exit(1)
	print("Basic Details has been created successfully")


def create_pumps():
	total = int(input("Enter total number of pumps to be created(e.g 4. enter a valid integer please): "))
	for i in range(total):
		print("Enter details for Pump "+ str(i))
		pump_logical_number = int(input("Enter pump logical number e.g 1, 2 - please enter valid integer: "))
		pump_type = input("Enter pump type e.g Kerosine, Petrol, Diesel: ")
		pump_maker = input("Enter pump maker e.g Gilbarco, lanfeng, tokheim etc: ")

		try:
			Pump.create(pump_logical_number=pump_logical_number,pump_type=pump_type,pump_maker=pump_maker)
		except Exception as e:
			print(e)
			print("Encountered an error creating pump. Please try again")
			sys.exit(1)

		print("Pump "+str(i)+" has been created successfully")

	print("Pumps have been created successfully")




def is_basic_config_set():
	get_one = Basic_details.select().where(Basic_details.id==1)
	if len(list(get_one)) == 1:
		return True
	else:
		return False

def is_pumps_set():
	get_all = Pump.select()
	if len(list(get_all)) < 1:
		return False
	else:
		return True

def sync_basic_data():
	if is_basic_config_set() is True:
		basic_det = Basic_details.get(Basic_details.id==1)
		details = pickle.dumps(basic_det)
		#change date to string
		print("Connecting to server")
		certificate = (CERT, KEY)
		try:
			result = requests.post(ADMIN_AUTH_SERVER+UPLOAD_BASIC_ROUTE,data=details, cert=certificate,verify=False)
		except Exception as e:
			print(e)
			print("Error connecting to Server. Please Try again")
			sys.exit(1)

		j_result = result.json()
		if j_result['has_uploaded'] == 'false':
			print("Error Uploading Information")
			sys.exit(1)
		elif j_result['has_uploaded'] == "exists":
			print("Basic details already exist on the server")

		elif j_result['has_uploaded'] == 'true':
			print("Basic Details has been uploaded successfully")
			return True

def sync_pump_data():
	if is_pumps_set() is True:
		pump = list(Pump.select())
		details = [model_to_dict(x) for x in pump]
		basic = list(Basic_details.select().where(Basic_details.id==1))

		#change datetime object to str
		for i in details:
			i['date_created'] = str(i['date_created'])
			i['token_permanent'] = basic[0].token_permanent
			del i['id']

		print("Connecting to server")
		certificate = (CERT, KEY)
		try:
			result = requests.post(ADMIN_AUTH_SERVER+UPLOAD_PUMP_ROUTE,json=details, cert=certificate,verify=False)
		except Exception as e:
			print(e)
			print("Error connecting to Server. Please Try again")
			sys.exit(1)

		j_result = result.json()
		if j_result['has_uploaded'] == 'some':
			print(j_result['msg'])
			sys.exit(1)
		elif j_result['has_uploaded'] == "all_exists":
			print(j_result['msg'])

		elif j_result['has_uploaded'] == 'true':
			print("Pump Data has been uploaded successfully")
			return True





if __name__ == '__main__':
	try:
		arg = sys.argv[1]
	except IndexError:
		command_list()
		sys.exit(1)
	else:
		run_command_from_arg(arg)
	

