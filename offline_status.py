from db.migrate import *
from queue import Queue
from constants import OFFLINE_STATUS_INTERVAL, OFFLINE_STATUS_QUEUE_SIZE
from threading import Thread
from time import sleep
from sock import Sock

import peewee
from playhouse.shortcuts import model_to_dict, dict_to_model


class MyDict(dict):
	pass

class Offline_Status:
	def __init__(self):
		self.status_queue = Queue(maxsize=OFFLINE_STATUS_QUEUE_SIZE)
		

	def offline_status_worker(self):
		worker = Thread(target=self.save_offline,args=())
		worker.setDaemon(True)
		worker.start()		
		print("Started Offline status Worker")

	def save_offline(self):
		while True:
			if not self.status_queue.empty():
				task = self.status_queue.get()
				if task.command == 'add':
					try:
						to_model = dict_to_model(Pump_status_unsent,task.message)
						to_model.save()
					except Exception as e:
						print(e)
				elif task.command == 'delete':
					try:
						to_model = dict_to_model(Pump_status_unsent,task.message)
						to_model.delete_instance()
					except Exception as e:
						print(e)

			sleep(OFFLINE_STATUS_INTERVAL)

	def get_unsent_status(self):
		status = list(Pump_status_unsent.select())
		if len(status) == 0:
			return False
		else:
			return [model_to_dict(i) for i in status]

	def delete_unsent_status(self,status_dict):
		status = MyDict()
		status.command = 'delete'
		status.message = status_dict
		try:
			self.status_queue.put(status)
		except Exception as e:
			print(e)



	def add_unsent_status(self,message):
		status = MyDict()
		status.command = 'add'
		status.message = message
		try:
			self.status_queue.put(status)
		except Exception as e:
			print(e)








