ADMIN_AUTH_SERVER = 'https://trident.node:1988'
AUTH_ROUTE = '/'
UPLOAD_BASIC_ROUTE = '/upload_basic_detail'
UPLOAD_PUMP_ROUTE = '/upload_pump_details'
SOCKS_SERVER = 'wss://trident.node:2008'

CERT = 'cert\\cert.pem'
KEY = 'cert\\key.pem'

INTERNET_CHECK_INTERVAL = 4
PTS_CHECK_INTERVAL = 10
DISPENSER_CHECK_INTERVAL=1
LOG_INTERVAL = 3
LOG_QUEUE_SIZE = 10
OFFLINE_STATUS_INTERVAL = 2
OFFLINE_STATUS_QUEUE_SIZE = 10
OFFLINE_STATUS_SEND_INTERVAL = 10


#server for checking internet connectivity
REMOTE_SERVER_FOR_PINGING = "www.google.com"

#parallel workers that handle sending of message to the server
STATUS_WORKERS = 4

#Queue size. Please as this goes up, so does memory usage goes up. Python has a funny way of not fully freeing memory
TASK_SIZE = 7