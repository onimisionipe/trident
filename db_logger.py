from db.migrate import *
from queue import Queue
from constants import LOG_INTERVAL, LOG_QUEUE_SIZE
from threading import Thread
from time import sleep


class Dict(dict):
	pass

class DBLogger:
	def __init__(self):
		self.log_queue = Queue(maxsize=LOG_QUEUE_SIZE)
		

	def log_worker(self):
		worker = Thread(target=self.add_log,args=())
		worker.setDaemon(True)
		worker.start()		
		print("Started Log Thread")

	def add_log(self):
		while True:
			if not self.log_queue.empty():
				task = self.log_queue.get()
				try:
					task.table.create(level=task.level, message=task.message)
				except Exception as e:
					pass
			sleep(LOG_INTERVAL)


	def internet_log(self,message, level="Info"):
		log = Dict()
		log.table = Internet_log
		log.level = level
		log.message = message
		try:
			self.log_queue.put(log)
		except Exception as e:
			pass

	def worker_log(self,message, level="Info"):
		log = Dict()
		log.table = Worker_log
		log.level = level
		log.message = message
		try:
			self.log_queue.put(log)
		except Exception as e:
			pass