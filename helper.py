from db.migrate import *
from config import is_basic_config_set, is_pumps_set
from trident_logger import *
import sys

log = Logger()

def get_pumps():
	if is_basic_config_set() is False:
		print("Please use the 'config' command to set basic configuration")
		log.logger.info("Configuration has not been set")
		return False
	if is_pumps_set() is False:
		print("Please use the 'config' command to create pumps")
		log.logger.info("Pump has not been set")
		return False

	return list(Pump.select())


def get_basic_config():
	if is_basic_config_set() is False:
		print("Please use the 'config' command to set basic configuration")
		log.logger.info("Configuration has not been set")
		return False
	return list(Basic_details.select().where(Basic_details.id==1))


def pump_status(status_no):
	statuses = ['offline','idle','work','trans_complete']
	if status_no == -1:
		return 'undefined'
	else:
		return statuses[status_no]

