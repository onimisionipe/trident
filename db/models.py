from peewee import *
from datetime import date
from datetime import datetime

db = SqliteDatabase('mydb.db')
db2 = SqliteDatabase('status_db.db')

class Basic_details(Model):
	station_name = CharField()
	address = TextField()
	state = CharField()
	city = CharField()
	branch_name = CharField()
	phone_number = CharField()
	number_of_pump = IntegerField()
	contact_person = CharField()
	contact_person_email = CharField()
	com_port_pts = IntegerField()
	token_permanent = CharField()
	date_started = DateField()

	class Meta:
		database = db


class Pump(Model):
	pump_logical_number = IntegerField()
	pump_type = CharField()
	pump_maker = CharField()
	date_created = DateField(default=date.today)

	class Meta:
		database = db

class Log(Model):
	date_created = DateTimeField(default=datetime.now)
	level = CharField()
	message = TextField()

	class Meta:
		database = db

class Internet_log(Model):
	date_created = DateTimeField(default=datetime.now)
	level = CharField()
	message = TextField()

	class Meta:
		database = db

class Worker_log(Model):
	date_created = DateTimeField(default=datetime.now)
	level = CharField()
	message = TextField()

	class Meta:
		database = db




class Cheat(Model):
	name = CharField()
	age = IntegerField()

	class Meta:
		database = db


class Pump_status_unsent(Model):
	token_permanent = CharField()
	status_date_time = DateTimeField()
	price = FloatField(null=True)
	nozzle = IntegerField()
	amount = FloatField(null=True)
	locked = BooleanField()
	pump = IntegerField()
	status = IntegerField()
	trans_no = IntegerField(null=True)
	pump_status_word = CharField()
	volume = FloatField(null=True)

	class Meta:
		database = db2
