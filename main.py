from pts import *

from helper import get_pumps, get_basic_config, pump_status
from trident_logger import Logger
from offline_status import *

from sock import Sock
from sock import TridentConnError

from time import sleep
from threading import Thread
from queue import Queue
import socket
import datetime
import pickle

from db_logger import DBLogger
from offline_status import Offline_Status
from constants import *


class Trident:
	def __init__(self):
		self.is_port_connected = False
		self.is_pts_connected = False
		self.is_internet_up = True
		self.pump_saved_state = {}
		self.db_logger = DBLogger()
		self.offline_status_sender = Offline_Status()
		#pumps
		self.pumps = get_pumps()

		
		#log
		self.log = Logger()
		#pump state Initialized
		self.__init_save_pumps_state()
		self.__initialize()
		self.db_logger.log_worker()
		self.offline_status_sender.offline_status_worker()
		#self.__connectivity_worker()		
		self.__init_status_workers()
		self.__init_send_offline_status_worker()
		self.__fetch_pump_status_worker()

	def __init_save_pumps_state(self):
		for p in get_pumps():

			dict_a = {}
			dict_a['price'] = None
			dict_a['nozzle'] = None
			dict_a['amount'] = None
			dict_a['locked'] = None
			dict_a['pump'] = p.pump_logical_number
			dict_a['status'] = None
			dict_a['trans_no'] = None
			dict_a['volume'] = None
			dict_a['pump_status_word'] = None
			self.pump_saved_state[p.pump_logical_number] = dict_a
		print(self.pump_saved_state)

	def __set_pump_state(self,pump_address, pump_state):
		self.pump_saved_state[pump_address] = pump_state

	def get_pump_state(self,pump_address):
		return self.pump_saved_state[pump_address]


	def __initialize(self):
		worker = Thread(target=self.connect_pts,args=())
		worker.setDaemon(True)
		worker.start()		
		self.log.logger.info("Started Worker that connects to PTS forever")

	def __connectivity_worker(self):
		worker = Thread(target=self.check_internet,args=())
		worker.setDaemon(True)
		worker.start()
		self.db_logger.worker_log("Connectivity worker has been started")

	def check_internet(self):
		print(self.is_internet_up)
		REMOTE_SERVER = REMOTE_SERVER_FOR_PINGING
		while True:
			try:
				host = socket.gethostbyname(REMOTE_SERVER)
				s = socket.create_connection((host, 80), 2)
				self.is_internet_up = True
			except:
				self.db_logger.internet_log("No Internet Connectivity", "error")
				self.is_internet_up = False
			sleep(INTERNET_CHECK_INTERVAL)
		


	def connect_pts(self):
		port = get_basic_config()[0].com_port_pts - 1
		self.prot = protocol(use_exceptions = 1)
		self.connect_port(port)		
		while True:
			
			if self.is_port_connected is True:
				try:
					self.prot.version_get()
					self.is_pts_connected = True
					print(self.is_pts_connected)
					print("is true")
				except Exception as e:
					self.is_pts_connected = False
					print(e)
					print(self.is_pts_connected)
					print("is false")

			sleep(PTS_CHECK_INTERVAL)

	def connect_port(self,port):
		try:
			self.prot.open(port)
			self.is_port_connected = True
			print(self.is_port_connected)
			print("port true")
		except Exception as e:
			self.log.logger.error(e)
			error = e.message
			if error.find("PermissionError") == -1:
				self.is_port_connected = False
			else:
				self.is_port_connected = True
			print(self.is_port_connected)


	def __fetch_pump_status_worker(self):
		worker = Thread(target=self.fetch_dispenser_status,args=())
		worker.setDaemon(True)
		worker.start()
		worker.join()
		self.log.logger.info("Started fetch pump status worker")

	def fetch_dispenser_status(self):
		while True:
	 		if self.is_pts_connected is True:
	 			for i in self.pumps:
	 				try:
	 					task = self.prot.status_request(i.pump_logical_number)
	 					self.__add_task(task)

	 				except Exception as e:
	 					self.db_logger.worker_log(e)
	 		sleep(DISPENSER_CHECK_INTERVAL)


	def __init_status_workers(self,size_of_queue=TASK_SIZE, no_of_workers=STATUS_WORKERS):
		self.__no_of_workers = no_of_workers
		self.__size_of_queue = size_of_queue
		self.queue = Queue(maxsize=self.__size_of_queue)
		
		self.__start_workers()

	def __init_send_offline_status_worker(self):
		worker = Thread(target=self.__send_offline,args=())
		worker.setDaemon(True)
		worker.start()		
		print("Started Offline status Worker for sending info to status server")

	def __send_offline(self):
		while True:
			if self.is_internet_up is True:
				if self.offline_status_sender.get_unsent_status() != False:
					offline_stat = self.offline_status_sender.get_unsent_status()
					for i in offline_stat:
						tmp = i.copy()
						del i['id']
						try:
							sender = Sock(self.db_logger)
							sender.send_status(pickle.dumps(i))
							self.offline_status_sender.delete_unsent_status(tmp)
						except Exception as e:
							print(e)

			sleep(OFFLINE_STATUS_SEND_INTERVAL)

	def __start_workers(self):
		for i in range(self.__no_of_workers):
			worker = Thread(target=self.__main_work,args=(self.queue,))
			worker.setDaemon(True)
			worker.start()
			print("Thread "+str(i))
			self.db_logger.worker_log("Started Worker "+str(i))

	def __main_work(self,q):
		basic_config_token = get_basic_config()[0].token_permanent
		#run an infinite loop to continue working
		while True:
			if not q.empty():
				task = q.get();
				task = self.serialize_status(task)
				if self.get_pump_state(task['pump']) == task:
					print('the same')
					#print(self.get_pump_state(task['pump']))

				else:
					self.__set_pump_state(task['pump'],task)
					print(self.get_pump_state(task['pump']))
					if self.is_internet_up == True:
						task_copy = task.copy()
						#add date and permanent token to the data to be sent to server
						task_copy['token_permanent'] = basic_config_token
						task_copy['status_date_time'] = datetime.datetime.today()
						print('before')
						print(task)
						try:
							sender = Sock(self.db_logger)
							stat = pickle.dumps(task_copy)
							print(task_copy)
							sender.send_status(stat.decode('Cp1252'))
							print('sent')
						except TridentConnError as e:
							#save for offline worker
							self.offline_status_sender.add_unsent_status(task_copy)
							del task_copy
					else:
						task_copy = task.copy()
						task_copy['token_permanent'] = basic_config_token
						task_copy['status_date_time'] = datetime.datetime.today()
						self.offline_status_sender.add_unsent_status(task_copy)
						del task_copy

						#save for offline worker
						

					#call task done to tell queue work is complete
					q.task_done()
			sleep(0.6)
			


	def __add_task(self,task):
		self.queue.put(task)

	def serialize_status(self,status):
		dict_a = {}
		dict_a['price'] = status.price()
		dict_a['nozzle'] = status.nozzle()
		dict_a['amount'] = status.amount()
		dict_a['locked'] = status.locked()
		dict_a['pump'] = status.pump()
		dict_a['status'] = status.status()
		dict_a['trans_no'] = status.trans_no()
		dict_a['volume'] = status.volume()
		dict_a['pump_status_word'] = pump_status(status.status())
		return dict_a
		


if __name__ == "__main__":
	t = Trident()
	
