import logging, logging.handlers

class Logger:
	def __init__(self):
		#disable python request logging
		logging.getLogger("requests").setLevel(logging.CRITICAL)
		
		self.init_logger()
		

	def init_logger(self):
		log_file_name = 'logs\\logs.log'
		handler = logging.handlers.RotatingFileHandler(log_file_name,maxBytes=200000,backupCount=100)
		logging.basicConfig(filename=log_file_name,level=logging.INFO,format='%(asctime)-15s  -  %(name)s  -  %(levelname)s  -   %(message)s')
		self.logger = logging.getLogger(__name__)
		self.logger.addHandler(handler)


	

	

