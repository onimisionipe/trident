import ssl

from websocket import create_connection
from constants import SOCKS_SERVER


class TridentConnError(Exception):
	def __init__(self,msg):
		self.msg = msg

class Sock:
	def __init__(self, logger):
		self.logger = logger
		try:
			self.socket = create_connection(SOCKS_SERVER,sslopt={"cert_reqs":ssl.CERT_NONE})
		except Exception as e:
			print(e)
			self.logger.internet_log("Failed to connect to Trident server","error")
			raise TridentConnError(str(e))

	def send_status(self,status):
		try:
			self.socket.send(status)
		except Exception as e:
			self.logger.internet_log("Failed to send message to Trident server","error")
			raise TridentConnError(str(e))

	def close_sock(self):
		try:
			self.socket.close()
		except Exception as e:
			pass

