# Make migrations for the dabatase models

from peewee import *
from db.models import *

db.connect()
mytables = [Pump_status_unsent]

def create_default():
	
	try:
		db.create_tables(mytables)
	except Exception as e:
		print(e)


def drop_default():
	try:
		db.drop_tables(mytables)
	except Exception as e:
		print(e)

