

### What is this repository for? ###

* Trident app was developed by Omega tech to help connect to PTS controller installed in a petrol station. This app gets feeds and status from fuel dispensers and sends them to the server. This app maintains a constant connection to the PTS controller to sync and send status.
* Version
v.1.0.3

### How do I get set up? ###

Install python and all necessary libraries and packages and run the app from the command line. or build the app with the necessary tools and install it directly on the system



### Who do I talk to? ###

* Onimisi Onipe <onimisionipe@gmail.com>