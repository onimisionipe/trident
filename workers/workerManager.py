from queue import Queue
from threading import Thread
from time import sleep
from db_logger import *
from constants import STATUS_WORKERS



class WorkerManager:
	def __init__(self,size_of_queue=, no_of_workers=STATUS_WORKERS):
		self.no_of_workers = no_of_workers;
		self.size_of_queue = size_of_queue;
		self.queue = Queue(maxsize=self.size_of_queue)
		
		self.__start_workers()



	def __start_workers(self):
		for i in range(self.no_of_workers):
			worker = Thread(target=self.work,args=(self.queue,))
			worker.setDaemon(True)
			worker.start()
			self.logger.info("Started Worker "+str(i))

	def work(self,q):
		#run an infinite loop to continue working
		while True:
			task = q.get();
			print(task['name'] + " will simulate blocking by sleeping for "+str(task['block'])+"secs")
			sleep(task['block'])

				#call task done to tell queue work is complete
			q.task_done()
			self.logger.info("Task "+task['name'] + " was performed successfully")


	def add_task(self,task):
		self.queue.put(task);
		self.logger.info("successfully added task")


"""
start = WorkerManager(0,3)
start.add_task({'name':'Matt','block':10})
start.add_task({'name':'Funke','block':5})
start.add_task({'name':'Tdon','block':1})
start.queue.join()
"""





